<?php
include "class/Film.php";
require_once "class/DBSingleton.php";

/*$response["page"]=$_POST["page"];
$response["num_rows"]=$_POST["num_rows"];
$response["rows_per_page"]=$_POST["rows_per_page"];*/
echo strlen((string)30);
switch ($_POST["method"]) {
    case 'listado':
        try {
            $cc = Singleton::getInstance();
            $stmt = $cc->getConnection()->prepare("SELECT * FROM film");
            $stmt->execute();
            while($datos=$stmt->fetch()){
                $films[]=new Film($datos["0"],$datos["1"],$datos["2"],$datos["3"],$datos["4"],$datos["5"],$datos["6"],$datos["7"],$datos["8"],$datos["9"],$datos["10"],$datos["11"],$datos["12"],$datos["13"]);
            } 
            $response["msg"]="Listado de las Peliculas.";
            $response["success"]=true;
            $response["data"]=$films;
        } catch (Exception $e) {
            $response["success"]=false;
        }
        echo json_encode($response); 
    break;
    case 'insert':
        try {
            $response["msg"]="Insertar.";
            $response["success"]=true;
            $instancia=new Film($_POST["film_id"], $_POST["title"], $_POST["description"], $_POST["release_year"], $_POST["language_id"], $_POST["original_language"], $_POST["rental_duration"], $_POST["rental_date"], $_POST["length"], $_POST["replacement_cost"], $_POST["rating"], $_POST["special_features"], $_POST["image"], $_POST["last_update"]);
            $instancia->insertFilm();  
        } catch (Exception $e) {
            $response["success"]=false;
        }  
        echo json_encode($response); 
    break;
    case 'delete':
        try {
            $response["msg"]="Eliminar.";
            $response["success"]=true;
            $cc = Singleton::getInstance();
            $stmt = $cc->getConnection()->prepare("DELETE FROM film where film_id=".$_POST["film_id"]);
            $stmt->execute();
        } catch (Exception $e) {
            $response["success"]=false;
        }  
        echo json_encode($response); 
    break;
    case 'update':
        try {
            $response["msg"]="Actualizar.";
            $response["success"]=true;
            $instancia=new Film($_POST["film_id"], $_POST["title"], $_POST["description"], $_POST["release_year"], $_POST["language_id"], $_POST["original_language"], $_POST["rental_duration"], $_POST["rental_date"], $_POST["length"], $_POST["replacement_cost"], $_POST["rating"], $_POST["special_features"], $_POST["image"], $_POST["last_update"]);
            $instancia->updateFilm();
        } catch (Exception $e) {
            $response["success"]=false;
        }  
        echo json_encode($response); 
    break;
    default:
        # code...
    break;
}