<?php 
require_once "DBSingleton.php";
class Film {
	//Atributos
	private $film_id;
	private $title;
	private $description;
	private $release_year;
	private $language_id;
	private $original_language;
	private $rental_duration;
 	private $rental_rate;
 	private $length;
 	private $replacement_cost;
 	private $rating;
 	private $special_features;
 	private $image;
 	private $last_update;

 	/**	
	  * 
	  */
 	function __construct($film_id, $title, $description, $release_year, $language_id, $original_language, $rental_duration, $rental_rate, $length, $replacement_cost, $rating, $special_features, $image, $last_update){
 		$this->setFilm_id($film_id);
 		$this->setTitle($title);
 		$this->setDescription($description);
 		$this->setRealease_year($release_year);
 		$this->setLanguage_id($language_id);
 		$this->setOriginal_language($original_language);
 		$this->setRental_duration($rental_duration);
 		$this->setRental_rate($rental_rate);
 		$this->setLength($length);
 		$this->setReplacement_cost($replacement_cost);
 		$this->setRating($rating);
 		$this->setSpecial_features($special_features);
 		$this->setImage($image);
 		$this->setLast_update($last_update);
 	}
 	//Destruct
 	function __destruct(){

 	}
 	//Getters y setters
 	public function getFilm_id(){
 		return $this->film_id;
 	}
 	public function setFilm_id($film_id){
		 if (strlen((string)$film_id)<=5){
			$this->film_i=$film_id;
		 }
 	}

 	public function getTitle(){
 		return $this->title;
 	}
 	public function setTitle($title){
		 if (strlen($title)<=255) {
			$this->titl=$title;
		 }
 	}

 	public function getDescription(){
 		return $this->description;
 	}
 	public function setDescription($description){
 		$this->descriptio=$description;
 	}

 	public function getRelease_year(){
 		return $this->release_year;
 	}
 	public function setRealease_year($release_year){
		if (strlen((string)$release_year<=4)){
			$this->release_yea=$release_year;
		}
 	}

 	public function getLanguage_id(){
 		return $this->language_id;
 	}
 	public function setLanguage_id($language_id){
		 if(strlen((string)$language_id)<=3){
			$this->language_i=$language_id;
		 }
 	}

 	public function getOriginal_language(){
 		return $this->original_language;
 	}
 	public function setOriginal_language($original_language){
		if(strlen((string)$original_language)<=3){ 
			$this->original_languag=$original_language;
		}
 	}

 	public function getRental_duration(){
 		return $this->rental_duration;
 	}
 	public function setRental_duration($rental_duration){
		if(strlen((string)$rental_duration)<=3){
			$this->rental_duratio=$rental_duration;
		} 
 	}

 	public function getRental_rate(){
 		return $this->rental_rate;
 	}
 	public function setRental_rate($rental_rate){
		 if(strlen((string)$rental_rate)<=4.2){
			$this->rental_rat=$rental_rate;
		 }
 	}

 	public function getLength(){
 		return $this->length;
 	}
 	public function setLength($length){
		if (strlen((string)$length)<=5) {
			$this->lengt=$length;
		}
 	}

 	public function getReplacement_cost(){
 		return $this->replacement_cost;
 	}
 	public function setReplacement_cost($replacement_cost){
		if(strlen((string)$replacement_cost)<=5.2){
			$this->replacement_cos=$replacement_cost;
		} 
 	}

 	public function getRating(){
 		return $this->rating;
 	}
 	public function setRating($rating){
		$arrayRatings = array("G", "PG", "PG-13", "R", "NC-17");
		foreach ($arrayRatings as $valor){
			if(strcmp($rating, $valor)==0){
				$this->ratin=$rating;
			}
		}
 	}

 	public function getSpecial_features(){
		return $this->special_features;
 	}
 	public function setSpecial_features($special_features){
		$arraySpecial_features = array("Trailers", "Commentaries", "Deleted Scenes", "Behind the Scenes");
		foreach ($arraySpecial_features as $valor){
			if (strcmp($special_features, $valor)==0){
				$this->special_feature=$special_features;
			}
		} 
 	}

 	public function getImage(){
 		return $this->image;
 	}
 	public function setImage($image){
		 if ((strlen($image))<=250){
			$this->imag=$image;
		 }
 	}

 	public function getLast_update(){
 		return $this->last_update;
 	}
 	public function setLast_update($last_update){
 		$this->last_updat=$last_update;
	 }
	//Insert
	public function insertFilm(){
		$cc=Singleton::getInstance();
        $sql="INSERT INTO film (film_id, title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features, image, last_update)
		VALUES(".$this->getFilm_id().", '".$this->getTitle()."', '".$this->getDescription()."','".$this->getRelease_year()."', ".$this->getLanguage_id().", ".$this->getOriginal_language().", ".$this->getRental_duration().", "
		.$this->getRental_rate().", ".$this->getLength().", ".$this->getReplacement_cost().", '".$this->getRating()."', '".$this->getSpecial_features()."', '".$this->getImage()."', '".$this->getLast_update()."')";
		$stmt=$cc->getConnection()->prepare($sql);
		$stmt->execute();
	}
	public function updateFilm(){
		$cc=Singleton::getInstance();
		$sql="UPDATE film SET title='".$this->getTitle()."', description='".$this->getDescription()."',release_year='".$this->getRelease_year()."', language_id=".$this->getLanguage_id().", original_language_id=".$this->getOriginal_language().
		", rental_duration=".$this->getRental_duration().", rental_rate=".$this->getRental_rate().", length=".$this->getLength().", replacement_cost=".$this->getReplacement_cost().", rating='".$this->getRating()."', special_features='".$this->getSpecial_features().
		"', image='".$this->getImage()."', last_update='".$this->getLast_update()."' WHERE film_id=".$this->getFilm_id();
		echo $sql;
		$stmt=$cc->getConnection()->prepare($sql);
        $stmt->execute();
	}
 }