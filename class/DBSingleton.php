<?php
class Singleton {
  private static $instance = null;
  private $conn;
  
  private $host = 'localhost';
  private $user = 'root';
  private $pass = '';
  private $dbName = 'sakila';
   
  private function __construct(){
    $this->conn = new PDO("mysql:host={$this->host};
    dbname={$this->dbName}", $this->user,$this->pass,
    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
  }
  
  public static function getInstance(){
    if(!self::$instance) {
      self::$instance = new Singleton();
    }
   
    return self::$instance;
  }
  
  public function getConnection(){
    return $this->conn;
  }
}
?>